MESMA Module
============

.. automodule:: mesma.core.mesma
    :members:
    :undoc-members:
    :show-inheritance:

.. argparse::
   :module: mesma.interfaces.mesma_cli
   :func: create_parser
   :prog: mesma