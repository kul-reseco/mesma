Soft To Hard Classification
===========================

.. automodule:: mesma.core.hard_classification
    :members:
    :undoc-members:
    :show-inheritance:

.. argparse::
   :module: mesma.interfaces.hard_classification_cli
   :func: create_parser
   :prog: mesma-classify
