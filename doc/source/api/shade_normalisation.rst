Shade Normalisation
===================

.. automodule:: mesma.core.shade_normalisation
    :members:
    :undoc-members:
    :show-inheritance:

.. argparse::
   :module: mesma.interfaces.shade_normalisation_cli
   :func: create_parser
   :prog: mesma-shade