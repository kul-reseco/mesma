MESMA Documentation
====================

.. figure:: userguide/images/mesma_illustrated.PNG
   :align: center
   :width: 80 %

.. toctree::
   :hidden:

   installation
   userguide
   exercises
   api

.. include:: ../../README.md

.. image:: lumos_big.svg
   :width: 40 %

.. image:: viper_big.svg
   :width: 40 %

.. image:: mesma.PNG
   :width: 15 %

Indexes
-------

* :ref:`genindex`
* :ref:`modindex`

