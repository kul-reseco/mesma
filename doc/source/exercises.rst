Exercises
=========

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/mesma/issues?status=new&status=open>`_.


We have developed two exercises. One with a data set in Brussels (Belgium), and one with a data set in Santa Barbara,
CA (USA).

These exercises consist of two parts: creating and optimizing spectral libraries
(visit http://spectral-libraries.readthedocs.io) and MESMA and post-processing: this part you will find here.


.. toctree::
   :maxdepth: 1

   exercises/exerciseBrussels
   exercises/exerciseSantaBarbara

