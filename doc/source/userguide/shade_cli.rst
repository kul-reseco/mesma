Command Line Interface
----------------------

The main command is::

        >mesma-shade

Use :code:`-h` or :code:`--help` to list all possible arguments::

        >mesma-shade -h

Only the **fractions image** is a **required argument**. It is the MESMA output image ending on *_fractions*. An example::

        >mesma-shade data\output\mesma_fractions

By default, the output file is stored in the same folder as the image file, with the extension '_normalised'.
To select another file or another location, use the argument :code:`-o` or :code:`--output`::

        >mesma-shade data\output\mesma_fractions -o data\other\normalised_image


**Issue Tracker:**

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/mesma/issues?status=new&status=open>`_.

