QGIS GUI
--------

.. figure:: images/hard_classification_gui.PNG
   :align: center
   :width: 70 %

#. Select the input SMA fraction image.

#. Optionally, the user can specify an output name.

The output image will contain a single band with numbers ranging from 0 to the number of classes minus one.
If you open the image in QGIS, the description (in the layer properties) will reveal what class each number stands for.

**Issue Tracker:**

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/mesma/issues?status=new&status=open>`_.

