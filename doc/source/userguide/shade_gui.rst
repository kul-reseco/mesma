QGIS GUI
--------

.. figure:: images/shade_gui.PNG
   :align: center
   :width: 70 %

#. Select the input SMA fraction image.

#. Optionally, the user can specify an output name.

The output image will contain one less band the input image since the *shade band* will no longer be present,
but the endmember fractions for each pixel will still sum to 1.


**Issue Tracker:**

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/mesma/issues?status=new&status=open>`_.

