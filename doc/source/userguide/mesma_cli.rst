Command Line Interface
----------------------

The main command is::

        >mesma

Use :code:`-h` or :code:`--help` to list all possible arguments::

        >mesma -h

Only the **spectral library** file, the **class name** and the **image** are **required arguments**. An example::

        >mesma data\spectral_library.sli LEVEL_2 data\image

All the other inputs are optional.

By default, the MESMA algorithm will run all 2- and 3-EM models. In case you would like to change this, use :code:`-l`
(the letter L, not number one) or :code:`--complexity-level`. Keep in mind that 1-EM models do not exist and the maximum number of levels depends on
the number of classes. An example::

        >mesma data\spectral_library.sli LEVEL_2 data\image -l 2 3 4

The default fusion threshold is 0.007. Models with a higher number of classes (e.g. 4-EM vs. 3-EM models) are only
chosen when the decrease in RMSE is larger than this threshold. In case you would like to change this,
use :code:`-f` or :code:`--fusion-threshold`::

        >mesma data\spectral_library.sli LEVEL_2 data\image -f 0.008

By default, the constraints are set at -0.05 (minimum endmember fraction), 1.05 (maximum endmember fraction),
0.00 (minimum shade fraction), 0.80 (maximum shade fraction) and 0.025 (maximum RMSE).
Use :code:`--min-fraction`, :code:`--max-fraction`, :code:`--min-shade-fraction`, :code:`--max-shade-fraction`
and :code:`--max-rmse` to change these values.

Residual constraints are not used by default. To enable them, use :code:`--residual-constraint`. This constraint
specifies number of consecutive bands that the residual values are allowed to exceed a given threshold. The default
values here are 0.025 (threshold) and 7 (number of consecutive bands). To change these values,
use :code:`--residual-constraint-values` and specify the two values in the order residual threshold and then number of bands.

To set no constraint for any given argument, use the value -9999.
To use no constraints at all, use the argument :code:`-u` or :code:`--unconstrained`::

        >mesma data\spectral_library.sli LEVEL_2 data\image --max-fraction 1.10 --max-rmse -9999
        >mesma data\spectral_library.sli LEVEL_2 data\image --residual-constraint --residual-constraint-values 0.02 10
        >mesma data\spectral_library.sli LEVEL_2 data\image -u

Use :code:`-a` or :code:`--shade` to set non-photometric shade. This is a spectral library with a single shade
spectrum::

        >mesma data\spectral_library.sli LEVEL_2 data\image -a data\shade_library

The reflectance scale of the image, spectral library and shade spectrum can all be automatically derived from the
values in the array as 1, 1 000 or 10 000. In case you would like to specify another scale,
use :code:`-r` or :code:`--reflectance-scale-library` to set the reflectance scale factor of the spectral library;
:code:`-s` or :code:`--reflectance-scale-image` to set the reflectance scale factor of the image;
and :code:`-t` or :code:`--reflectance-scale-shade` to set the reflectance scale factor of the shade spectrum::

        >mesma data\spectral_library.sli LEVEL_2 data\image --r 255 --s 255

By default, the output file is stored in the same folder as the image file, with the extension '_mesma_' followed by
a timestamp. To select another file or another location, use the argument :code:`-o` or :code:`--output`::

        >mesma data\spectral_library.sli LEVEL_2 data\image -o "data\subfolder\mesma_image"

It is also possible to create a residual image as part of the output. By default this is disabled. To turn the option
on, use :code:`-d` or :code:`--residuals-image`::

        >mesma data\spectral_library.sli LEVEL_2 data\image -d

Lastly, there are 2 advance MESMA algorithms part of the tool: Spectral Weighing and Band Selection (=Stable Zone
Unmixing). To turn on Spectral Weighing, use :code:`--spectral-weighing`. To turn on Stable Zone Unmixing, use
:code:`--band-selection`. The Stable Zone Unmixing algorithm also required two input variables: the correlation
threshold (default 0.99) and the correlation decrease (default 0.01). To change these values,
use :code:`--band-selection-values` to specify the tuple (correlation threshold, correlation decrease)::

        >mesma data\spectral_library.sli LEVEL_2 data\image --band-selection --band-selection-values 0.80 0.05


**Issue Tracker:**

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/mesma/issues?status=new&status=open>`_.

