Post-processing
---------------

Shade Normalisation of SMA product
..................................

Shade normalization can be performed on SMA fraction images to assess relative abundance of surface materials with
the shade component removed.  For example, assume we are interested in the relative abundance of green
vegetation in a semi-arid region with high topographic relief.  In the standard SMA outputs the contribution
of topographic shade may cause some confusion when interpreting the green vegetation abundance values.
If comparing the green vegetation fractions between a region with low local incidence angle and a region with
high local incidence angle, the region of low local incidence will show a higher green vegetation fraction due to
the reduced shade component, even if the green vegetation abundance is the same as the other site. Shade
normalization is particularly valuable in urban areas, where dark impervious surface is often mapped as having a
high shade fraction. These effects can be removed by shade normalizing the SMA output images.

.. toctree::
   :maxdepth: 1

   shade_gui
   shade_cli

Soft to Hard Classification of SMA product
..........................................

The SMA fraction image gives a very detailed insight in the sub-pixel fractions in the images landscape.
This tool is more suited for user interested in a hard classification,
assigning the class of the most abundant fraction (e.g. GV or SOIL, but excluding shade) to any given pixel.

The output image will contain a single band with numbers ranging from 0 to the number of classes minus one.
If you open the image in QGIS, the description (in the layer properties) will reveal what class each number stands for.


.. toctree::
   :maxdepth: 1

   hard_classification_gui
   hard_classification_cli


**ACKNOWLEDGMENTS**

This user guide is based on the VIPER Tools 2.0 user guide (UC Santa Barbara, VIPER Lab):
Roberts, D. A., Halligan, K., Dennison, P., Dudley, K., Somers, B., Crabbe, A., 2018, Viper Tools User Manual,
Version 2, 91 pp.

