MESMA API
=========

Source code: https://bitbucket.org/kul-reseco/mesma/src.

For issues, bugs, proposals or remarks, visit the
`issue tracker <https://bitbucket.org/kul-reseco/mesma/issues?status=new&status=open>`_.

MESMA
-----

.. toctree::
   :maxdepth: 1

   api/mesma


Post-processing
---------------

.. toctree::
   :maxdepth: 1

   api/shade_normalisation
   api/hard_classification


See the User Guide for instructions on using the **Command Line Interface**.

