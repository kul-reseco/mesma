# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : March 2020
| Copyright           : © 2020 by Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the MESMA plugin and python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
import os
import numpy as np
from mesma.interfaces.mesma_cli import create_parser, run_mesma
from mesma.interfaces.imports import import_image
from tests import ExtendedUnitTesting

precision = 5
folder = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data, folder. for; testing')
idl_output_folder = os.path.join(folder, 'output_mesma')


class TestMesmaCLI(ExtendedUnitTesting):
    image_path = os.path.join(folder, "test-data")
    library_path = os.path.join(folder, "roberts2017_urban_subset.sli")
    metadata = "LEVEL_2"
    shade_path = os.path.join(folder, "shade_wrong.sli")
    output_path = os.path.join(folder, "cli_mesma_output")

    @classmethod
    def tearDownClass(cls):
        files = [cls.output_path,
                 '{}_fractions'.format(cls.output_path),
                 '{}_rmse'.format(cls.output_path),
                 '{}_residuals'.format(cls.output_path)]

        for file in files:
            out1 = file
            out2 = '{}.hdr'.format(os.path.splitext(out1)[0])
            out3 = '{}.aux.xml'.format(out1)
            if os.path.exists(out1):
                os.remove(out1)
            if os.path.exists(out2):
                os.remove(out2)
            if os.path.exists(out3):
                os.remove(out3)

    def test_23_constrained(self):
        parser = create_parser()
        run_mesma(parser.parse_args([self.library_path, self.metadata, self.image_path,
                                     '-o={}'.format(self.output_path), '--min-fraction=0', '--max-fraction=1',
                                     '--max-shade-fraction=0.5', '--max-rmse=0.05', '--residual-constraint',
                                     '--residual-constraint-values', '0.02', '10', '-c', '4']))
        # library: library_path, class: LEVEL 2, image: image_path, output: output_path
        # complexity_level: 2+3, reflectance_scale_library: None, reflectance_scale_image: None, fusion: 0.007
        # unconstrained: F, min_fraction: 0, max_fraction: 1, min_shade: 0, max_shade: 0.5, max_rmse: 0.05
        # residual_constraint: T, residual_values: 0.02 + 10, shade: None, reflectance_scale_shade: None
        # residual_image: F, spectral_weighing: F, band_selection: F, band_selection_values: 0.99+0.01

        models, rmse, frac = self.get_mesma_results(self.output_path)

        # get IDL version data
        idl_models, idl_level, idl_rmse, idl_frac, idl_lut = \
            self.import_idl_results(file_path=os.path.join(idl_output_folder, "mesma_IDL_2EM_constrained"))

        # compare RMSE
        self.assertEqual(rmse.shape, idl_rmse.shape)
        rmse[np.where(rmse == 9999)] = 0
        self.assertEqualFloatArray(rmse, idl_rmse, 4)

        # compare FRACTIONS
        self.assertEqual(frac.shape, idl_frac.shape)
        self.assertEqualFloatArray(frac, idl_frac, 4)

        # compare LEVEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                python_level = len(np.where(models[:, i, j] != -1)[0])
                self.assertEqual(python_level, idl_level[i][j])

        # compare MODEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                current_model = models[:, i, j]
                current_model_ind = np.where(current_model != -1)[0]
                current_model_str = ', '.join(str(x) for x in current_model[current_model_ind])
                if idl_models[i][j] == 0:
                    idl_model_str = ''
                else:
                    idl_model_str = idl_lut[idl_models[i][j] - 1]
                self.assertEqual(current_model_str, idl_model_str)

    def test_23_fusion009(self):
        parser = create_parser()
        run_mesma(parser.parse_args([self.library_path, self.metadata, self.image_path,
                                     '-o={}'.format(self.output_path), '-u', '-f=0.009', '-c', '4']))
        # library: library_path, class: LEVEL 2, image: image_path, output: output_path
        # complexity_level: 2+3, reflectance_scale_library: None, reflectance_scale_image: None, fusion: 0.009
        # unconstrained: T, min_fraction: -0.05, max_fraction: 1.05, min_shade: 0, max_shade: 0.5, max_rmse: 0.025
        # residual_constraint: F, residual_values: 0.025 + 7, shade: None, reflectance_scale_shade: None
        # residual_image: F, spectral_weighing: F, band_selection: F, band_selection_values: 0.99+0.01

        models, rmse, frac = self.get_mesma_results(self.output_path)

        # get IDL version data
        idl_models, idl_level, idl_rmse, idl_frac, idl_lut = \
            self.import_idl_results(file_path=os.path.join(idl_output_folder, "mesma_IDL_2EM_fusion009"))

        # compare RMSE
        self.assertEqual(rmse.shape, idl_rmse.shape)
        rmse[np.where(rmse == 9999)] = 0
        self.assertEqualFloatArray(rmse, idl_rmse, 4)

        # compare FRACTIONS
        self.assertEqual(frac.shape, idl_frac.shape)
        self.assertEqualFloatArray(frac, idl_frac, 4)

        # compare LEVEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                python_level = len(np.where(models[:, i, j] != -1)[0])
                self.assertEqual(python_level, idl_level[i][j])

        # compare MODEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                current_model = models[:, i, j]
                current_model_ind = np.where(current_model != -1)[0]
                current_model_str = ', '.join(str(x) for x in current_model[current_model_ind])
                idl_model_str = idl_lut[idl_models[i][j] - 1]
                self.assertEqual(current_model_str, idl_model_str)

    def test_23_shade(self):
        parser = create_parser()
        run_mesma(parser.parse_args([self.library_path, self.metadata, self.image_path, '-u',
                                     '-o={}'.format(self.output_path), '-a={}'.format(self.shade_path), '-c', '4']))
        # library: library_path, class: LEVEL 2, image: image_path, output: output_path
        # complexity_level: 2+3, reflectance_scale_library: None, reflectance_scale_image: None, fusion: 0.007
        # unconstrained: T, min_fraction: -0.05, max_fraction: 1.05, min_shade: 0, max_shade: 0.5, max_rmse: 0.025
        # residual_constraint: F, residual_values: 0.025 + 7, shade: shade_path, reflectance_scale_shade: None
        # residual_image: F, spectral_weighing: F, band_selection: F, band_selection_values: 0.99+0.01

        models, rmse, frac = self.get_mesma_results(self.output_path)

        # get IDL version data
        idl_models, idl_level, idl_rmse, idl_frac, idl_lut = \
            self.import_idl_results(file_path=os.path.join(idl_output_folder, "mesma_IDL_3EM_shade"))

        # compare RMSE
        self.assertEqual(rmse.shape, idl_rmse.shape)
        rmse[np.where(rmse == 9999)] = 0
        self.assertEqualFloatArray(rmse, idl_rmse, 4)

        # compare FRACTIONS
        self.assertEqual(frac.shape, idl_frac.shape)
        self.assertEqualFloatArray(frac, idl_frac, 4)

        # compare LEVEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                python_level = len(np.where(models[:, i, j] != -1)[0])
                self.assertEqual(python_level, idl_level[i][j])

        # compare MODEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                current_model = models[:, i, j]
                current_model_ind = np.where(current_model != -1)[0]
                current_model_str = ', '.join(str(x) for x in current_model[current_model_ind])
                idl_model_str = idl_lut[idl_models[i][j] - 1]
                self.assertEqual(current_model_str, idl_model_str)

    def test_3_band_weighing(self):
        parser = create_parser()
        run_mesma(parser.parse_args([self.library_path, self.metadata, self.image_path,
                                     '-o={}'.format(self.output_path), '-l', '3', '--spectral-weighing', '-c', '4']))
        # library: library_path, class: LEVEL 2, image: image_path, output: output_path
        # complexity_level: 3, reflectance_scale_library: None, reflectance_scale_image: None, fusion: 0.007
        # unconstrained: F, min_fraction: -0.05, max_fraction: 1.05, min_shade: 0, max_shade: 0.8, max_rmse: 0.025
        # residual_constraint: F, residual_values: 0.025 + 7, shade: None, reflectance_scale_shade: None
        # residual_image: F, spectral_weighing: T, band_selection: F, band_selection_values: 0.99+0.01

        models, rmse, frac = self.get_mesma_results(self.output_path)

        # get IDL version data
        idl_models, idl_level, idl_rmse, idl_frac, idl_lut = \
            self.import_idl_results(file_path=os.path.join(idl_output_folder, "mesma_IDL_3EM_band_weighing"))

        # compare RMSE
        self.assertEqual(rmse.shape, idl_rmse.shape)
        rmse[np.where(rmse == 9999)] = 0
        self.assertEqualFloatArray(rmse, idl_rmse, 4)

        # compare FRACTIONS
        frac = frac[[0, 1, 2, 3, 4, 5, 7, 8], :, :]
        self.assertEqual(frac.shape, idl_frac.shape)
        self.assertEqualFloatArray(frac, idl_frac, 4)

        # compare LEVEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                python_level = len(np.where(models[:, i, j] != -1)[0])
                self.assertEqual(python_level, idl_level[i][j])

        # compare MODEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                current_model = models[:, i, j]
                current_model_ind = np.where(current_model != -1)[0]
                current_model_str = ', '.join(str(x) for x in current_model[current_model_ind])
                if idl_models[i][j] == 0:
                    idl_model_str = ''
                else:
                    idl_model_str = idl_lut[idl_models[i][j] - 1]
                self.assertEqual(current_model_str, idl_model_str)

    def test_23_residuals(self):
        parser = create_parser()
        run_mesma(parser.parse_args([self.library_path, self.metadata, self.image_path,
                                     '-o={}'.format(self.output_path), '-d', '-c', '4']))

        models, rmse, frac = self.get_mesma_results(self.output_path)
        residuals = import_image('{}_residuals'.format(self.output_path))

        # get IDL version data
        idl_models, idl_level, idl_rmse, idl_frac, idl_lut = \
            self.import_idl_results(file_path=os.path.join(idl_output_folder, "mesma_IDL_3EM_residuals"))
        idl_residuals = import_image(os.path.join(idl_output_folder, "mesma_IDL_3EM_residuals_residuals"))

        # compare RMSE
        self.assertEqual(rmse.shape, idl_rmse.shape)
        rmse[np.where(rmse == 9999)] = 0
        self.assertEqualFloatArray(rmse, idl_rmse, 4)

        # compare FRACTIONS
        self.assertEqual(frac.shape, idl_frac.shape)
        self.assertEqualFloatArray(frac, idl_frac, 4)

        # compare LEVEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                python_level = len(np.where(models[:, i, j] != -1)[0])
                self.assertEqual(python_level, idl_level[i][j])

        # compare MODEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                current_model = models[:, i, j]
                current_model_ind = np.where(current_model != -1)[0]
                current_model_str = ', '.join(str(x) for x in current_model[current_model_ind])
                if idl_models[i][j] == 0:
                    idl_model_str = ''
                else:
                    idl_model_str = idl_lut[idl_models[i][j] - 1]
                self.assertEqual(current_model_str, idl_model_str)

        # compare residuals
        self.assertEqual(residuals.shape, idl_residuals.shape)
        self.assertEqualFloatArray(residuals, idl_residuals, 4)
