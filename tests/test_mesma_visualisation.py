# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : March 2020
| Copyright           : © 2020 by Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the MESMA plugin and python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
import os
import re
import copy
import pickle
import numpy as np

from osgeo import gdal_array
from qgis.gui import QgsMapCanvas
from qgis.core import QgsRasterRenderer, QgsRasterBlock, QgsRasterLayer, QgsMapSettings
from qgis.core import QgsMapRendererParallelJob
from qgis.PyQt.Qt import QAbstractItemModel, QModelIndex
from qgis.PyQt.QtGui import QColor
from qgis.PyQt.QtCore import QTimer

from mesma.interfaces.mesma_visualisation_gui import MESMA_CLASS_UNMODELED_VALUE, MesmaModel, MesmaTreeView, MesmaTreeModel
from mesma.interfaces.mesma_visualisation_gui import MesmaSummary, ModelImageRenderer, ModelVisualizationWidget
from mesma.interfaces.mesma_visualisation_gui import SortFilterProxyModel
from tests import ExtendedUnitTesting, app


MODELS_NAME = "mesma_output_models"  # used library is "roberts2017_urban_subset.sli"
RASTER_NAME = "test-data"


class TestMesmaVisualisation(ExtendedUnitTesting):
    """ Test Class for the MESMA visualisation tool. """

    @classmethod
    def setUpClass(cls):
        """ Set up test class. """
        folder = os.path.join(os.path.dirname(__file__), 'data, folder. for; testing')
        cls.modelsPath = os.path.join(folder, MODELS_NAME)
        cls.rasterPath = os.path.join(folder, RASTER_NAME)

    @staticmethod
    def inMemoryModel():
        """ Create a Memory Model Image. """

        path = r'/vsimem/mesma_model.bsq'
        class_names = ['gv', 'npv', 'water']
        ns = 100
        nl = 50
        nb = len(class_names)
        data_ignore_value = -9999
        array = np.random.randint(0, 100, size=(nb, nl, ns), dtype=np.int32) - 50

        # ignore some classes
        array = np.where(array < 0, np.ones((nb, nl, ns), dtype=np.int32) * MESMA_CLASS_UNMODELED_VALUE, array)

        # add ignored pixels
        array[:, -10:-1, 0:20] = data_ignore_value

        ds = gdal_array.SaveArray(array, path, format='ENVI')

        for c, className in enumerate(class_names):
            band = ds.GetRasterBand(c + 1)
            band.SetDescription(className)

        ds.FlushCache()
        return path

    @staticmethod
    def getColorNames(raster_block: QgsRasterBlock) -> list:
        """
        Returns the color names of an QgsRasterBlock

        :param raster_block: QgsRasterBlock
        :return: [list-of-str]
        """
        rgb_colors = set()
        for c in range(raster_block.width()):
            for r in range(raster_block.height()):
                color = raster_block.color(r, c)
                rgb_colors.add(color)
        colors = [QColor.fromRgba(rgb).name() for rgb in rgb_colors]
        return colors

    def test_MesmaModel(self):

        endmembers = np.asarray([1, 2, 3, 4, 5])

        info1 = MesmaModel(vector=endmembers, color=None, name=None)
        self.assertIsInstance(info1.color(), QColor)
        self.assertIsInstance(info1.name(), str)
        self.assertEqual(info1.name(), info1.vectorString())
        self.assertIsInstance(info1.classIndices(), np.ndarray)
        info1.setVisible(True)
        self.assertEqual(info1.isVisible(), True)
        info1.setPxCount(10)
        self.assertEqual(info1.pxCount(), 10)
        info1.setName("Model")
        self.assertEqual(info1.name(), "Model")
        self.assertEqual(info1.numberOfClasses(), len(endmembers))

        info2 = MesmaModel(vector=endmembers, color=None, name=None)
        self.assertEqual(info1, info2)

        info_copy = copy.copy(info1)
        self.assertEqual(info_copy, info1)

        dump = pickle.dumps(info1)
        info3 = pickle.loads(dump)
        self.assertIsInstance(info3, MesmaModel)
        self.assertEqual(info1, info3)

    def test_MesmaModel_from_summary(self):
        summary = MesmaSummary()
        summary.readMesmaModelsImage(parent=None, path=self.modelsPath, testing=True)

        models = summary[:]
        self.assertIsInstance(models, list)
        self.assertTrue(len(models) > 0)

        for model in models:
            self.assertIsInstance(model, MesmaModel)
            vector_string = model.vectorString()
            self.assertIsInstance(vector_string, str)
            self.assertTrue(re.search(r'((\d|_),*)+', vector_string))
            vector_string2 = summary.usedClassesString(model)
            self.assertIsInstance(vector_string2, str)

    def test_ModelImageRenderer(self):

        renderer = ModelImageRenderer()
        self.assertIsInstance(renderer, QgsRasterRenderer)

        path = self.inMemoryModel()
        lyr = QgsRasterLayer(path, 'TEST', 'gdal')
        ext = lyr.extent()

        renderer.setInput(lyr.dataProvider())
        self.assertTrue(len(renderer._models) == 0)

        width = 126
        height = 256

        block = renderer.block(0, ext, width, height)
        self.assertIsInstance(block, QgsRasterBlock)
        colors = self.getColorNames(block)
        self.assertTrue(len(colors) == 1)
        self.assertEqual(colors[0], renderer._no_data_color.name())

        renderer.setInput(lyr.dataProvider())

        summary = MesmaSummary()
        summary.readMesmaModelsImage(parent=None, path=path, testing=True)
        mesma_models = summary.mesmaModels()
        for i in range(10):
            mesma_models[i].setVisible(True)
        renderer.setModels(summary.visibleModels())

        block = renderer.block(0, ext, width, height)
        self.assertIsInstance(block, QgsRasterBlock)
        colors = self.getColorNames(block)
        self.assertTrue(len(colors) > 1)
        self.assertEqual(colors[0], renderer._no_data_color.name())

        symbols = renderer.legendSymbologyItems()
        self.assertIsInstance(symbols, list)
        self.assertEqual(len(symbols), len(renderer._models))
        for s in symbols:
            self.assertIsInstance(s, tuple)
            self.assertIsInstance(s[0], str)
            self.assertIsInstance(s[1], QColor)

        dump = pickle.dumps(renderer)

        r2 = pickle.loads(dump)
        self.assertIsInstance(r2, ModelImageRenderer)
        self.assertListEqual(renderer._models, r2._models)

    def test_MesmaTreeModel(self):
        p = self.modelsPath
        self.assertTrue(os.path.isfile(p))
        summary = MesmaSummary()
        tm = MesmaTreeModel(summary)
        self.assertIsInstance(tm, QAbstractItemModel)

        view = MesmaTreeView()
        view.show()

        proxy_model = SortFilterProxyModel()
        proxy_model.setSourceModel(tm)
        view.setModel(proxy_model)

        summary.readMesmaModelsImage(parent=None, path=self.modelsPath, testing=True)
        self.assertTrue(tm.rowCount(QModelIndex()) == 2)
        self.assertTrue(tm.columnCount() > 1)

        QTimer.singleShot(1000, app.closeAllWindows)
        app.exec_()

    def test_rasterBlock(self):

        lyr = QgsRasterLayer(self.rasterPath)
        width = 200
        height = 150
        extent = lyr.extent()
        block = lyr.dataProvider().block(1,extent,width, height)
        self.assertIsInstance(block, QgsRasterBlock)

        npx = height * width
        nb = lyr.bandCount()
        block_data = None
        for b in range(nb):
            band_block = lyr.dataProvider().block(b + 1, extent, width, height)
            assert isinstance(band_block, QgsRasterBlock)
            if block_data is None:
                block_data = np.ndarray((nb, npx), dtype=gdal_array.GDALTypeCodeToNumericTypeCode(band_block.dataType()))

            for i in range(npx):
                block_data[b, i] = band_block.value(i)

    def test_widget(self):
        widget = ModelVisualizationWidget(testing=True)
        widget.show()

        self.assertIsInstance(widget._tree_model.rasterRenderer(), ModelImageRenderer)
        widget.openImage([self.modelsPath])

        layer = widget._endMemberRasterLayer()
        self.assertIsInstance(layer, QgsRasterLayer)

        # widget.loadTestData()
        QTimer.singleShot(2000, app.closeAllWindows)
        app.exec_()

    def test_referenceRenderer(self):

        mc = QgsMapCanvas()
        settings = mc.mapSettings()
        self.assertIsInstance(settings, QgsMapSettings)

        job = QgsMapRendererParallelJob(settings)

    def test_modelSummary(self):
        p = self.modelsPath
        self.assertTrue(os.path.isfile(p))
        summary = MesmaSummary()
        summary.clear()
        emitted_signals = set()

        def _checkSignal(name):
            emitted_signals.add(name)

        summary.sigLoadingProgress.connect(lambda : _checkSignal('before'))
        summary.sigLoadingProgress.connect(lambda : _checkSignal('progress'))
        summary.sigSourceChanged.connect(lambda : _checkSignal('changed'))
        summary.readMesmaModelsImage(parent=None, path=p, testing=True)

        self.assertTrue('before' in emitted_signals)
        self.assertTrue('progress' in emitted_signals)
        self.assertTrue('changed' in emitted_signals)

        colors = [m.color() for m in summary.classMemberships()]
        self.assertIsInstance(colors, list)
        self.assertTrue(len(colors) > 0 and len(colors) == len(summary._class_names))
        for color in colors:
            self.assertIsInstance(color, QColor)
