# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : March 2020
| Copyright           : © 2020 by Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the MESMA plugin and python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
import os
import numpy as np
from mesma.interfaces.imports import import_image
from mesma.interfaces.hard_classification_cli import create_parser, run_hard_classification
from tests import ExtendedUnitTesting

folder = os.path.join(os.path.dirname(__file__), 'data, folder. for; testing')


class TestHardClassificationCLI(ExtendedUnitTesting):
    image_path = os.path.join(folder, 'mesma_output_fractions')
    output_path = os.path.join(folder, 'normalisation_test_cli')
    default_output = os.path.join(folder, 'mesma_output_fractions_classification')

    @classmethod
    def setUpClass(cls):
        parser = create_parser()
        run_hard_classification(parser.parse_args([cls.image_path, '-o={}'.format(cls.output_path)]))
        cls.classified_image = import_image(cls.output_path)

        # test 2 default output
        run_hard_classification(parser.parse_args([cls.image_path]))

    @classmethod
    def tearDownClass(cls):
        files = [cls.output_path, '{}.hdr'.format(os.path.splitext(cls.output_path)[0]),
                 '{}.aux.xml'.format(cls.output_path), '{}.aux.xml'.format(cls.image_path),
                 cls.default_output, '{}.hdr'.format(os.path.splitext(cls.default_output)[0]),
                 '{}.aux.xml'.format(cls.default_output)]
        for file in files:
            if os.path.exists(file):
                os.remove(file)

    def test_shape(self):
        self.assertEqual(self.classified_image.shape, (50, 50))
        self.assertEqual(np.min(self.classified_image), 0)
        self.assertEqual(np.max(self.classified_image), 7)

    def test_row0(self):
        validation = np.array([0, 0, 0, 0, 0, 1, 0, 4, 3, 0, 0, 2, 2, 0, 4, 0, 3, 0, 0, 0, 1, 0, 0, 0, 0,
                               4, 4, 1, 4, 5, 4, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 4, 0, 1, 2, 2, 0, 0, 0])
        self.assertEqual(validation.shape, self.classified_image[0, :].shape)
        self.assertEqualIntArray(validation, self.classified_image[0, :])

    def test_row35(self):
        validation = np.array([3, 3, 0, 0, 0, 2, 3, 0, 1, 5, 4, 3, 3, 0, 0, 0, 2, 2, 3, 3, 3, 3, 3, 3, 3,
                               0, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 2, 0, 0, 0, 0, 2, 0, 5, 5, 2, 3])
        self.assertEqual(validation.shape, self.classified_image[34, :].shape)
        self.assertEqualIntArray(validation, self.classified_image[34, :])

    def test_default_output(self):
        self.assertTrue(os.path.exists(self.default_output))
