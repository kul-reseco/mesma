# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : March 2020
| Copyright           : © 2020 by Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the MESMA plugin and python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
import os
import numpy as np
from mesma.core.mesma import MesmaModels
from mesma.interfaces.imports import import_library
from tests import ExtendedUnitTesting

# console settings (wide output)
np.set_printoptions(linewidth=140)
folder = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data, folder. for; testing')


class TestMesma(ExtendedUnitTesting):

    @classmethod
    def setUpClass(cls):
        # library
        library = import_library(os.path.join(folder, "roberts2017_urban_subset.sli"))
        cls.class_list = np.asarray([x.metadata("LEVEL_2") for x in library.profiles()], dtype=str)

    def test_models(self):
        models_object = MesmaModels()
        models_object.setup(self.class_list)
        lut_a = models_object.return_look_up_table()

        self.assertEqual(len(lut_a[2]), 8)
        self.assertEqual(len(lut_a[3]), 28)

        models_object.select_model(False, index=0, level=2)
        lut_b = models_object.return_look_up_table()

        self.assertEqual(len(lut_b[2]), 7)

        total = models_object.total()
        total_level_2 = models_object.total_per_level(2)
        total_class_x = models_object.total_per_class_model((0, 1))
        total_not_existing_level = models_object.total_per_level(1000)
        max_digits = models_object.max_digits()
        summary = models_object.summary()
        summary = summary.splitlines()
        save = models_object.save()
        save = save.splitlines()

        self.assertEqual(total, 1433)
        self.assertEqual(total_level_2, 48)
        self.assertEqual(total_class_x, 60)
        self.assertEqual(total_not_existing_level, 0)
        self.assertEqual(max_digits, 13)
        self.assertRegex(summary[0], 'You selected:')
        self.assertRegex(summary[1], 'Total number of models: ')
        self.assertRegex(save[0], 'x-EM Levels:*')
        self.assertRegex(save[2], 'classes per x-EM Level:*')
