# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : March 2020
| Copyright           : © 2020 by Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the MESMA plugin and python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
import os
import numpy as np
from mesma.core.mesma import MesmaCore, MesmaModels
from mesma.interfaces.imports import import_library, import_image
from tests import ExtendedUnitTesting

folder = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data, folder. for; testing')
idl_output_folder = os.path.join(folder, 'output_mesma')


class TestMesma(ExtendedUnitTesting):
    test_data = os.path.join(folder, "test-data")
    roberts2017 = os.path.join(folder, "roberts2017_urban_subset.sli")
    r_class = "LEVEL_2"
    shade = os.path.join(folder, "shade_wrong.sli")

    @classmethod
    def setUpClass(cls):
        # image
        cls.image = import_image(cls.test_data) / 10000

        # library
        library = import_library(cls.roberts2017)
        cls.library = np.array([np.array(x.values()['y'])[np.where(x.bbl())[0]] for x in library.profiles()]).T / 10000
        class_list = np.asarray([x.metadata(cls.r_class) for x in library.profiles()], dtype=str)

        # shade
        shade = import_library(cls.shade)
        cls.shade = np.array([np.array(x.values()['y'])[np.where(x.bbl())[0]] for x in shade.profiles()]).T / 10000

        # models
        models_object_23 = MesmaModels()
        models_object_23.setup(class_list)
        cls.lut_23 = models_object_23.return_look_up_table()
        cls.em_23 = models_object_23.em_per_class
        n_classes = len(cls.em_23)

        models_object_234 = MesmaModels()
        models_object_234.setup(class_list)
        models_object_234.select_level(True, 4)
        for i in np.arange(n_classes):
            models_object_234.select_class(True, i, 4)
        cls.lut_234 = models_object_234.return_look_up_table()
        cls.em_234 = models_object_234.em_per_class

        models_object_3 = MesmaModels()
        models_object_3.setup(class_list)
        models_object_3.select_level(state=False, level=2)
        models_object_3.select_class(state=False, index=6, level=3)
        cls.lut_3 = models_object_3.return_look_up_table()
        cls.em_3 = models_object_3.em_per_class

    @classmethod
    def tearDownClass(cls) -> None:
        pass

    def test_234_unconstrained(self):
        models, fractions, rmse, _ = MesmaCore(4).execute(image=self.image, library=self.library,
                                                          em_per_class=self.em_234,
                                                          look_up_table=self.lut_234,
                                                          constraints=[-9999, -9999, -9999, -9999, -9999, -9999, -9999])

        # get IDL version data
        idl_model_nr, idl_level, idl_rmse, idl_fractions, idl_model_lut = \
            self.import_idl_results(os.path.join(idl_output_folder, "mesma_IDL_3EM_unconstrained"))

        # compare RMSE
        self.assertEqual(rmse.shape, idl_rmse.shape)
        rmse[np.where(rmse == 9999)] = 0
        self.assertEqualFloatArray(rmse, idl_rmse, 4)

        # compare FRACTIONS
        self.assertEqual(fractions.shape, idl_fractions.shape)
        self.assertEqualFloatArray(fractions, idl_fractions, 4)

        # compare LEVEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                python_level = len(np.where(models[:, i, j] != -1)[0])
                self.assertEqual(python_level, idl_level[i][j])

        # compare MODEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                current_model = models[:, i, j]
                current_model_ind = np.where(current_model != -1)[0]
                current_model_str = ', '.join(str(x) for x in current_model[current_model_ind])
                idl_model_str = idl_model_lut[idl_model_nr[i][j] - 1]
                self.assertEqual(current_model_str, idl_model_str)

    def test_23_no_data_pixels(self):
        models, fractions, rmse, _ = MesmaCore(4).execute(image=self.image,
                                                          no_data_pixels=(np.array([0, 0, 0]), np.array([0, 1, 2])),
                                                          library=self.library, look_up_table=self.lut_23,
                                                          em_per_class=self.em_23,
                                                          constraints=[-9999, -9999, -9999, -9999, -9999, -9999, -9999])

        # get IDL version data
        idl_model_nr, idl_level, idl_rmse, idl_fractions, idl_model_lut = \
            self.import_idl_results(os.path.join(idl_output_folder, "mesma_IDL_2EM_unconstrained"))

        # compare RMSE
        self.assertEqual(rmse.shape, idl_rmse.shape)
        self.assertTrue(rmse[0, 0] == 9998)
        self.assertTrue(rmse[0, 1] == 9998)
        self.assertTrue(rmse[0, 2] == 9998)
        self.assertEqualFloatArray(rmse[0, 3:], idl_rmse[0, 3:], 4)
        self.assertEqualFloatArray(rmse[1:, :], idl_rmse[1:, :], 4)

    def test_23_residuals(self):
        models, fractions, rmse, residuals = MesmaCore(4).execute(image=self.image, library=self.library,
                                                                  look_up_table=self.lut_23,
                                                                  em_per_class=self.em_23, residual_image=True)

        # get IDL version data
        idl_model_nr, idl_level, idl_rmse, idl_fractions, idl_model_lut = \
            self.import_idl_results(os.path.join(idl_output_folder, "mesma_IDL_3EM_residuals"))
        idl_residuals = import_image(os.path.join(idl_output_folder, "mesma_IDL_3EM_residuals_residuals"))

        # compare RMSE
        self.assertEqual(rmse.shape, idl_rmse.shape)
        rmse[np.where(rmse == 9999)] = 0
        self.assertEqualFloatArray(rmse, idl_rmse, 4)

        # compare FRACTIONS
        self.assertEqual(fractions.shape, idl_fractions.shape)
        self.assertEqualFloatArray(fractions, idl_fractions, 4)

        # compare LEVEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                python_level = len(np.where(models[:, i, j] != -1)[0])
                self.assertEqual(python_level, idl_level[i][j])

        # compare MODEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                current_model = models[:, i, j]
                current_model_ind = np.where(current_model != -1)[0]
                current_model_str = ', '.join(str(x) for x in current_model[current_model_ind])
                if idl_model_nr[i][j] == 0:
                    idl_model_str = ''
                else:
                    idl_model_str = idl_model_lut[idl_model_nr[i][j] - 1]
                self.assertEqual(current_model_str, idl_model_str)

        # compare residuals
        self.assertEqual(residuals.shape, idl_residuals.shape)
        self.assertEqualFloatArray(residuals, idl_residuals, 4)

    def test_23_shade(self):
        models, fractions, rmse, _ = MesmaCore(4).execute(image=self.image,
                                                          library=self.library, look_up_table=self.lut_23,
                                                          em_per_class=self.em_23, shade_spectrum=self.shade,
                                                          constraints=[-9999, -9999, -9999, -9999, -9999, -9999, -9999])

        # get IDL version data
        idl_model_nr, idl_level, idl_rmse, idl_fractions, idl_model_lut = \
            self.import_idl_results(os.path.join(idl_output_folder, "mesma_IDL_3EM_shade"))

        # compare RMSE
        self.assertEqual(rmse.shape, idl_rmse.shape)
        rmse[np.where(rmse == 9999)] = 0
        self.assertEqualFloatArray(rmse, idl_rmse, 4)

        # compare FRACTIONS
        self.assertEqual(fractions.shape, idl_fractions.shape)
        self.assertEqualFloatArray(fractions, idl_fractions, 4)

        # compare LEVEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                python_level = len(np.where(models[:, i, j] != -1)[0])
                self.assertEqual(python_level, idl_level[i][j])

        # compare MODEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                current_model = models[:, i, j]
                current_model_ind = np.where(current_model != -1)[0]
                current_model_str = ', '.join(str(x) for x in current_model[current_model_ind])
                idl_model_str = idl_model_lut[idl_model_nr[i][j] - 1]
                self.assertEqual(current_model_str, idl_model_str)

    def test_3_band_selection(self):
        models, fractions, rmse, _ = MesmaCore(4).execute(image=self.image, library=self.library,
                                                          look_up_table=self.lut_3,
                                                          em_per_class=self.em_3,
                                                          constraints=[-0.05, 1.05, 0, 0.8, 0.025, -9999, -9999],
                                                          use_band_selection=True)

        # get IDL version data
        idl_model_nr, idl_level, idl_rmse, idl_fractions, idl_model_lut = \
            self.import_idl_results(os.path.join(idl_output_folder, "mesma_IDL_3EM_band_selection"))

        # compare RMSE
        self.assertEqual(rmse.shape, idl_rmse.shape)
        rmse[np.where(rmse == 9999)] = 0
        self.assertEqualFloatArray(rmse, idl_rmse, 4)

        # compare FRACTIONS
        fractions = fractions[[0, 1, 2, 3, 4, 5, 7, 8], :, :]
        self.assertEqual(fractions.shape, idl_fractions.shape)
        self.assertEqualFloatArray(fractions, idl_fractions, 4)

        # compare LEVEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                python_level = len(np.where(models[:, i, j] != -1)[0])
                self.assertEqual(python_level, idl_level[i][j])

        # compare MODEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                current_model = models[:, i, j]
                current_model_ind = np.where(current_model != -1)[0]
                current_model_str = ', '.join(str(x) for x in current_model[current_model_ind])
                if idl_model_nr[i][j] == 0:
                    idl_model_str = ''
                else:
                    idl_model_str = idl_model_lut[idl_model_nr[i][j] - 1]
                self.assertEqual(current_model_str, idl_model_str)

    def test_3_band_weighing(self):
        models, fractions, rmse, _ = MesmaCore(4).execute(image=self.image, library=self.library,
                                                          look_up_table=self.lut_3,
                                                          em_per_class=self.em_3,
                                                          constraints=[-0.05, 1.05, 0, 0.8, 0.025, -9999, -9999],
                                                          use_band_weighing=True)

        # get IDL version data
        idl_model_nr, idl_level, idl_rmse, idl_fractions, idl_model_lut = \
            self.import_idl_results(os.path.join(idl_output_folder, "mesma_IDL_3EM_band_weighing"))

        # compare RMSE
        self.assertEqual(rmse.shape, idl_rmse.shape)
        rmse[np.where(rmse == 9999)] = 0
        self.assertEqualFloatArray(rmse, idl_rmse, 4)

        # compare FRACTIONS
        fractions = fractions[[0, 1, 2, 3, 4, 5, 7, 8], :, :]
        self.assertEqual(fractions.shape, idl_fractions.shape)
        self.assertEqualFloatArray(fractions, idl_fractions, 4)

        # compare LEVEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                python_level = len(np.where(models[:, i, j] != -1)[0])
                self.assertEqual(python_level, idl_level[i][j])

        # compare MODEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                current_model = models[:, i, j]
                current_model_ind = np.where(current_model != -1)[0]
                current_model_str = ', '.join(str(x) for x in current_model[current_model_ind])
                if idl_model_nr[i][j] == 0:
                    idl_model_str = ''
                else:
                    idl_model_str = idl_model_lut[idl_model_nr[i][j] - 1]
                self.assertEqual(current_model_str, idl_model_str)

    def test_3_band_weighing_selection(self):
        models, fractions, rmse, _ = MesmaCore(4).execute(image=self.image, library=self.library,
                                                          look_up_table=self.lut_3,
                                                          em_per_class=self.em_3,
                                                          constraints=[-0.05, 1.05, 0, 0.8, 0.025, -9999, -9999],
                                                          use_band_weighing=True, use_band_selection=True)

        # get IDL version data
        idl_model_nr, idl_level, idl_rmse, idl_fractions, idl_model_lut = \
            self.import_idl_results(os.path.join(idl_output_folder, "mesma_IDL_3EM_band_weighing_selection"))

        # compare RMSE
        self.assertEqual(rmse.shape, idl_rmse.shape)
        rmse[np.where(rmse == 9999)] = 0
        self.assertEqualFloatArray(rmse, idl_rmse, 4)

        # compare FRACTIONS
        fractions = fractions[[0, 1, 2, 3, 4, 5, 7, 8], :, :]
        self.assertEqual(fractions.shape, idl_fractions.shape)
        self.assertEqualFloatArray(fractions, idl_fractions, 4)

        # compare LEVEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                python_level = len(np.where(models[:, i, j] != -1)[0])
                self.assertEqual(python_level, idl_level[i][j])

        # compare MODEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                current_model = models[:, i, j]
                current_model_ind = np.where(current_model != -1)[0]
                current_model_str = ', '.join(str(x) for x in current_model[current_model_ind])
                if idl_model_nr[i][j] == 0:
                    idl_model_str = ''
                else:
                    idl_model_str = idl_model_lut[idl_model_nr[i][j] - 1]
                self.assertEqual(current_model_str, idl_model_str)
