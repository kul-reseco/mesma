# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : March 2020
| Copyright           : © 2020 by Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the MESMA plugin and python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
import os
import numpy as np
from qgis.PyQt.QtCore import QTimer
from qgis.core import QgsRasterLayer, QgsProject
from mesma.interfaces.imports import import_image
from mesma.interfaces.shade_normalisation_gui import ShadeNormalisationWidget
from tests import ExtendedUnitTesting, app

DECIMALS = 5
folder = os.path.join(os.path.dirname(__file__), 'data, folder. for; testing')


class TestShadeNormalisationGUI(ExtendedUnitTesting):
    image_path = os.path.join(folder, 'mesma_output_fractions')
    output_path1 = os.path.join(folder, 'classified')
    output_path2 = os.path.join(folder, 'not_here')

    @classmethod
    def setUpClass(cls):
        # test 1 normal
        widget = ShadeNormalisationWidget()
        layer = QgsRasterLayer(cls.image_path, os.path.basename(cls.image_path), 'gdal')
        QgsProject.instance().addMapLayer(layer, True)
        widget.inputComboBox.setLayer(layer)
        widget.outputFileWidget.lineEdit().setText(cls.output_path1)
        widget._run_shade_normalisation()
        QgsProject.instance().removeMapLayer(layer)
        widget.close()

        cls.new_fractions = import_image(cls.output_path1)

        # # test 2 no shade band                                     # test only works locally
        # widget = ShadeNormalisationWidget()
        # sys.stdout = sys.__stdout__
        # sys.stderr = sys.__stderr__
        # layer = QgsRasterLayer(cls.output_path1, os.path.basename(cls.output_path1), 'gdal')
        # QgsProject.instance().addMapLayer(layer, True)
        # widget.inputComboBox.setLayer(layer)
        # widget.outputFileWidget.lineEdit().setText(cls.output_path2)
        # widget._run_shade_normalisation()
        # QgsProject.instance().removeMapLayer(layer)
        # widget.close()

        # # test 3 default output                                    # test only works locally
        # widget = ShadeNormalisationWidget()
        # sys.stdout = sys.__stdout__
        # sys.stderr = sys.__stderr__
        # layer = QgsRasterLayer(cls.image_path, os.path.basename(cls.image_path), 'gdal')
        # QgsProject.instance().addMapLayer(layer, True)
        # widget.inputComboBox.setLayer(layer)
        # widget.openInQGIS.setChecked(True)
        # widget._run_shade_normalisation()
        # QgsProject.instance().removeMapLayer(layer)
        # widget.close()

    @classmethod
    def tearDownClass(cls):
        files = [cls.output_path1,
                 '{}.hdr'.format(os.path.splitext(cls.output_path1)[0]),
                 '{}.aux.xml'.format(cls.output_path1),
                 '{}.aux.xml'.format(cls.image_path)]
        for file in files:
            if os.path.exists(file):
                os.remove(file)

    def assertEqualFloatArray(self, array1, array2, decimals):
        if array1.ndim == 1:
            pix = array1.shape[0]
        elif array1.ndim == 2:
            pix = array1.shape[0] * array1.shape[1]
        elif array1.ndim == 3:
            pix = array1.shape[0] * array1.shape[1] * array1.shape[2]
        else:
            assert 0

        array1_line = np.reshape(array1, pix)
        array2_line = np.reshape(array2, pix)
        for i in range(pix):
            self.assertAlmostEqual(array1_line[i], array2_line[i], places=decimals)

    def test_row0(self):
        idl_row0 = np.array([[0.6797764, 1.0, 1.0, 1.0, 1.0, 0.37529492, 0.6779559, 0.0, 0.0, 0.61495215, 0.61907566, 0.25807753, 0.1633783, 1.0, 0.49563986, 0.63861424, 0.4330713, 1.0, 1.0, 0.5244497, 0.46877936, 0.6048232, 0.7683161, 1.0, 0.5538147, 0.46230045, 0.0, 0.0, 0.49010476, 0.29668236, 0.3626939, 0.42885008, 0.5410209, 0.7358865, 1.0, 0.0, 0.0, 0.16072297, 1.0, 0.29969278, 1.0, 0.23447765, 0.49080527, 0.708869, 0.39222446, 0.3802804, 0.420401, 0.52785414, 1.0, 1.0],
                             [0.0, 0.0, 0.0, 0.0, 0.0, 0.6247051, 0.32204407, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.4755502, 0.5312207, 0.3951768, 0.23168397, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.5711499, 0.45897907, 0.0, 0.0, 1.0, 1.0, 0.839277, 0.0, 0.70030725, 0.0, 0.76552236, 0.0, 0.29113105, 0.6077755, 0.0, 0.0, 0.0, 0.0, 0.0],
                             [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.74192244, 0.8366217, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.61971956, 0.579599, 0.0, 0.0, 0.0],
                             [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.77549034, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5669287, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.43716705, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                             [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5007676, 0.22450969, 0.38504785, 0.3809244, 0.0, 0.0, 0.0, 0.50436014, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.4461853, 0.5376995, 0.5628329, 0.0, 0.50989527, 0.0, 0.63730603, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5091948, 0.0, 0.0, 0.0, 0.0, 0.47214583, 0.0, 0.0],
                             [0.3202236, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.49923247, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.36138576, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.70331764, 0.0, 0.0, 0.0, 0.2641135, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                             [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                             [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]])

        self.assertEqual(idl_row0.shape, self.new_fractions[:, 0, :].shape)
        self.assertEqualFloatArray(idl_row0, self.new_fractions[:, 0, :], DECIMALS)

    def test_row10(self):
        idl_row10 = np.array([[0.54881465, 0.0, 0.14082165, 1.0, 0.48649183, 1.0, 0.6358872, 0.42288345, 0.6832363, 0.54855543, 0.72102076, 0.50994235, 1.0, 0.4786614, 0.6399332, 0.49378547, 0.6934863, 1.0, 0.41144708, 0.75847805, 0.57154244, 0.4486887, 1.0, 1.0, 0.9135795, 0.73672485, 0.0, 1.0, 0.45884037, 0.57853925, 0.8146676, 0.6601373, 0.5465664, 0.6424061, 0.65603936, 1.0, 0.90751374, 0.72820807, 0.57394874, 0.48405161, 0.6122377, 0.54755753, 0.18432964, 1.0, 0.0, 0.4051604, 0.237217, 0.2875627, 0.08301014, 0.0],
                              [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.3641128, 0.57711655, 0.0, 0.45144457, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.34396064, 0.0, 0.0, 0.0, 0.0, 0.5159484, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                              [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.31676376, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5062145, 0.3065137, 0.0, 0.5885529, 0.24152198, 0.42845756, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.91698986, 0.0],
                              [0.0, 1.0, 0.85917836, 0.0, 0.51350814, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.4459653, 0.0, 0.0, 0.0, 0.0, 0.0, 0.4534335, 0.3575939, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.8156703, 0.0, 0.0, 0.0, 0.76278305, 0.0, 0.0, 1.0],
                              [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.27897924, 0.0, 0.0, 0.5213386, 0.36006677, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.086420484, 0.26327515, 0.5540347, 0.0, 0.0, 0.4214608, 0.18533239, 0.3398627, 0.0, 0.0, 0.0, 0.0, 0.09248629, 0.27179193, 0.42605126, 0.0, 0.38776234, 0.45244247, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                              [0.45118532, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.4900576, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5513114, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5411597, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5948396, 0.0, 0.71243733, 0.0, 0.0],
                              [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                              [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]])

        self.assertEqual(idl_row10.shape, self.new_fractions[:, 10, :].shape)
        self.assertEqualFloatArray(idl_row10, self.new_fractions[:, 10, :], DECIMALS)

    def test_row45(self):
        idl_row45 = np.array([[0.32417992, 0.6967367, 0.73591477, 0.6115415, 1.0, 0.0, 0.0, 0.0, 0.0, 0.16894023, 0.0, 0.0, 0.07124037, 0.3414074, 0.0, 0.0, 0.08309727, 0.0, 0.0, 0.0, 0.08980849, 0.0, 0.41026872, 0.61032397, 0.51104325, 0.72245497, 1.0, 0.5014761, 0.12066908, 0.88207996, 1.0, 0.6722575, 0.69625425, 0.6938752, 1.0, 0.8805658, 0.42670637, 1.0, 0.5068549, 0.0, 0.0, 0.4118707, 0.4374649, 0.17621265, 0.0, 0.55234396, 0.0, 0.0, 0.08657288, 0.1608078],
                              [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.4985239, 0.8793309, 0.11792001, 0.0, 0.3277425, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                              [0.0, 0.0, 0.0, 0.38845843, 0.0, 0.0, 1.0, 1.0, 1.0, 0.83105975, 0.0, 1.0, 0.92875963, 0.0, 1.0, 1.0, 0.9169027, 1.0, 0.8439576, 0.0, 0.9101915, 1.0, 0.0, 0.0, 0.0, 0.277545, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.30612475, 0.0, 0.0, 0.5732936, 0.0, 0.0, 0.0, 0.0, 0.58812934, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.9134271, 0.0],
                              [0.6758201, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.48895672, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.82378733, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                              [0.0, 0.30326334, 0.2640853, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5897313, 0.3896761, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.30374578, 0.0, 0.0, 0.119434156, 0.0, 0.0, 0.49314508, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.44765604, 1.0, 1.0, 0.0, 0.8391922],
                              [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.65859264, 0.0, 0.0, 0.0, 0.0, 0.15604241, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5625351, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                              [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                              [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]])

        self.assertEqual(idl_row45.shape, self.new_fractions[:, 45, :].shape)
        self.assertEqualFloatArray(idl_row45, self.new_fractions[:, 45, :], DECIMALS)

    # def test_no_shade_band(self):                                  # test only works locally
    #     self.assertFalse(os.path.exists(self.output_path2))
    #
    # def test_default_output(self):                                 # test only works locally
    #     self.assertTrue(len(QgsProject.instance().mapLayers()) == 1)

    def test_app_opens(self):
        z = ShadeNormalisationWidget()
        z.show()

        QTimer.singleShot(1000, app.closeAllWindows)
        app.exec_()
