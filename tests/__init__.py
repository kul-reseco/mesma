# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : March 2020
| Copyright           : © 2020 by Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the MESMA plugin and python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
import csv
import numpy as np
import unittest

from mesma.interfaces.imports import import_image

from qgis.core import QgsApplication
app = QgsApplication([], True)
app.initQgis()


class ExtendedUnitTesting(unittest.TestCase):

    def assertEqualFloatArray(self, array1, array2, decimals):
        if array1.ndim == 1:
            pix = array1.shape[0]
        elif array1.ndim == 2:
            pix = array1.shape[0] * array1.shape[1]
        elif array1.ndim == 3:
            pix = array1.shape[0] * array1.shape[1] * array1.shape[2]
        else:
            assert 0

        array1_line = np.reshape(array1, pix)
        array2_line = np.reshape(array2, pix)
        for i in range(pix):
            self.assertAlmostEqual(array1_line[i], array2_line[i], places=decimals)

    def assertEqualIntArray(self, array1, array2):
        if array1.ndim == 1:
            pix = array1.shape[0]
        elif array1.ndim == 2:
            pix = array1.shape[0] * array1.shape[1]
        elif array1.ndim == 3:
            pix = array1.shape[0] * array1.shape[1] * array1.shape[2]
        else:
            assert 0

        array1_line = np.reshape(array1, pix)
        array2_line = np.reshape(array2, pix)
        for i in range(pix):
            self.assertEqual(array1_line[i], array2_line[i])

    def assertEqualStringArray(self, array1, array2):
        x = array1.shape[0]
        for i in range(x):
            self.assertEqual(array1[i], array2[i])

    @staticmethod
    def import_idl_results(file_path):

        image = import_image(file_path)

        models_path = file_path + "_models.csv"
        with open(models_path) as open_file:
            csv_reader = csv.reader(open_file)
            models = list(csv_reader)

        header = models[0]
        halfway = int((len(header) - 5) / 2) + 3
        models.remove(header)
        models = np.array(models, dtype=str)
        models = models[:, halfway:-2]
        models_string = list()
        for row in models:
            row = filter(None, row)
            text = ", ".join(row)
            models_string.append(text)
        if 'band' not in file_path:
            idl_model_nr = np.array(image[11, :, :], dtype=np.int32)
            idl_level = np.array(image[10, :, :], dtype=np.int32)
            idl_rmse = image[9, :, :]
            idl_fractions = image[0:9, :, :]
        else:
            idl_model_nr = np.array(image[10, :, :], dtype=np.int32)
            idl_level = np.array(image[9, :, :], dtype=np.int32)
            idl_rmse = image[8, :, :]
            idl_fractions = image[0:8, :, :]

        return idl_model_nr, idl_level, idl_rmse, idl_fractions, models_string

    @staticmethod
    def get_mesma_results(models_path):
        models = import_image(models_path)
        rmse = import_image('{}_rmse'.format(models_path))
        fractions = import_image('{}_fractions'.format(models_path))
        return models, rmse, fractions
