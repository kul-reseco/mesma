# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : March 2020
| Copyright           : © 2020 by Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the MESMA plugin and python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
import os
import numpy as np
from mesma.core.mesma import MesmaCore, MesmaModels
from mesma.interfaces.imports import import_library, import_image
from tests import ExtendedUnitTesting

folder = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data, folder. for; testing')
idl_output_folder = os.path.join(folder, 'output_mesma')


class TestMesma(ExtendedUnitTesting):

    @classmethod
    def setUpClass(cls):
        # image
        image = import_image(os.path.join(folder, "test-data")) / 10000

        # library
        library_obj = import_library(os.path.join(folder, "roberts2017_urban_subset.sli"))
        library = np.array([np.array(x.values()['y'])[np.where(x.bbl())[0]] for x in library_obj.profiles()]).T / 10000
        class_list = np.asarray([x.metadata("LEVEL_2") for x in library_obj.profiles()], dtype=str)

        # models
        models_object_23 = MesmaModels()
        models_object_23.setup(class_list)
        lut_23 = models_object_23.return_look_up_table()
        em_23 = models_object_23.em_per_class

        cls.result = MesmaCore(4).execute(image=image, library=library, look_up_table=lut_23, em_per_class=em_23,
                                          constraints=[0, 1, 0, 0.5, 0.05, 0.02, 10])

    def test_23_constrained(self):

        models, fractions, rmse, _ = self.result

        # get IDL version data
        idl_model_nr, idl_level, idl_rmse, idl_fractions, idl_model_lut = \
            self.import_idl_results(os.path.join(idl_output_folder, "mesma_IDL_2EM_constrained"))

        # compare RMSE
        self.assertEqual(rmse.shape, idl_rmse.shape)
        rmse[np.where(rmse == 9999)] = 0
        self.assertEqualFloatArray(rmse, idl_rmse, 4)

        # compare FRACTIONS
        self.assertEqual(fractions.shape, idl_fractions.shape)
        self.assertEqualFloatArray(fractions, idl_fractions, 4)

        # compare LEVEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                python_level = len(np.where(models[:, i, j] != -1)[0])
                self.assertEqual(python_level, idl_level[i][j])

        # compare MODEL
        for i in np.arange(models.shape[1]):
            for j in np.arange(models.shape[2]):
                current_model = models[:, i, j]
                current_model_ind = np.where(current_model != -1)[0]
                current_model_str = ', '.join(str(x) for x in current_model[current_model_ind])
                if idl_model_nr[i][j] == 0:
                    idl_model_str = ''
                else:
                    idl_model_str = idl_model_lut[idl_model_nr[i][j] - 1]
                self.assertEqual(current_model_str, idl_model_str)
