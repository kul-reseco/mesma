# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : March 2020
| Copyright           : © 2020 by Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the MESMA plugin and python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
import os
import numpy as np
from mesma.core.mesma import MesmaCore, MesmaModels
from mesma.interfaces.imports import import_library, import_image
from tests import ExtendedUnitTesting

folder = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data, folder. for; testing')


class TestMesma(ExtendedUnitTesting):

    @classmethod
    def setUpClass(cls):
        # image
        cls.image = import_image(os.path.join(folder, "test-data")) / 10000

        # library
        library = import_library(os.path.join(folder, "roberts2017_urban_subset.sli"))
        cls.library = np.array([np.array(x.values()['y'])[np.where(x.bbl())[0]] for x in library.profiles()]).T / 10000
        class_list = np.asarray([x.metadata("LEVEL_2") for x in library.profiles()], dtype=str)

        # models
        models_object_23 = MesmaModels()
        models_object_23.setup(class_list)
        cls.lut_23 = models_object_23.return_look_up_table()
        cls.em_23 = models_object_23.em_per_class

    def test_values_larger_than_one(self):
        image = self.image * 10000
        library = self.library * 10000

        with self.assertRaises(ValueError):
            MesmaCore(4).execute(image=image, library=library, look_up_table=self.lut_23, em_per_class=self.em_23)
        with self.assertRaises(ValueError):
            MesmaCore(4).execute(image=self.image, library=library, look_up_table=self.lut_23, em_per_class=self.em_23)
