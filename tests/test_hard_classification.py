# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : March 2020
| Copyright           : © 2020 by Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the MESMA plugin and python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
import os
import numpy as np
from mesma.interfaces.imports import import_image
from mesma.core.hard_classification import HardClassification
from tests import ExtendedUnitTesting

folder = os.path.join(os.path.dirname(__file__), 'data, folder. for; testing')


class TestHardClassification(ExtendedUnitTesting):

    @classmethod
    def setUpClass(cls):
        image = import_image(os.path.join(folder, 'mesma_output_fractions'))
        cls.classified_image = HardClassification().execute(image)

    def test_shape(self):
        self.assertEqual(self.classified_image.shape, (50, 50))
        self.assertEqual(np.min(self.classified_image), 0)
        self.assertEqual(np.max(self.classified_image), 7)

    def test_row0(self):
        validation = np.array([0, 0, 0, 0, 0, 1, 0, 4, 3, 0, 0, 2, 2, 0, 4, 0, 3, 0, 0, 0, 1, 0, 0, 0, 0,
                               4, 4, 1, 4, 5, 4, 1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 4, 0, 1, 2, 2, 0, 0, 0])
        self.assertEqual(validation.shape, self.classified_image[0, :].shape)
        self.assertEqualIntArray(validation, self.classified_image[0, :])

    def test_row35(self):
        validation = np.array([3, 3, 0, 0, 0, 2, 3, 0, 1, 5, 4, 3, 3, 0, 0, 0, 2, 2, 3, 3, 3, 3, 3, 3, 3,
                               0, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 2, 0, 0, 0, 0, 2, 0, 5, 5, 2, 3])
        self.assertEqual(validation.shape, self.classified_image[34, :].shape)
        self.assertEqualIntArray(validation, self.classified_image[34, :])
