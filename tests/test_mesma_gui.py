# -*- coding: utf-8 -*-
"""
| ----------------------------------------------------------------------------------------------------------------------
| Date                : March 2020
| Copyright           : © 2020 by Ann Crabbé (KU Leuven)
| Email               : acrabbe.foss@gmail.com
|
| This file is part of the MESMA plugin and python package.
|
| This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
| License as published by the Free Software Foundation, either version 3 of the License, or any later version.
|
| This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
| warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
|
| You should have received a copy of the GNU General Public License (COPYING.txt). If not see www.gnu.org/licenses.
| ----------------------------------------------------------------------------------------------------------------------
"""
import os
import numpy as np
from qgis.PyQt.QtCore import QTimer
from qgis.gui import QgsFileWidget
from mesma.interfaces.mesma_gui import MesmaWidget, MesmaModelsWidget, MesmaModels, LevelWidget
from mesma.interfaces.imports import import_library
from tests import ExtendedUnitTesting, app

folder = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'data, folder. for; testing')


class TestMesmaGUI(ExtendedUnitTesting):
    test_data1 = os.path.join(folder, "test-data")
    test_data2 = os.path.join(folder, "test-data2")
    roberts2017 = os.path.join(folder, "roberts2017_urban_subset.sli")
    r_class = "LEVEL_2"
    shade = os.path.join(folder, "shade_wrong.sli")
    output = None

    @classmethod
    def setUpClass(cls):
        library = import_library(cls.roberts2017)
        class_list = np.asarray([x.metadata(cls.r_class) for x in library.profiles()], dtype=str)

        # models
        cls.models_object = MesmaModels()
        cls.models_object.setup(class_list)

    @classmethod
    def tearDownClass(cls):
        all_files = []
        output = QgsFileWidget.splitFilePaths(cls.output)
        for file in output:
            all_files.append(file)
            all_files.append('{}.hdr'.format(file))
            all_files.append('{}.aux.xml'.format(file))
            all_files.append('{}_fractions'.format(file))
            all_files.append('{}_fractions.hdr'.format(file))
            all_files.append('{}_fractions.aux.xml'.format(file))
            all_files.append('{}_rmse'.format(file))
            all_files.append('{}_rmse.hdr'.format(file))
            all_files.append('{}_rmse.aux.xml'.format(file))

        for file in all_files:
            if os.path.exists(file):
                os.remove(file)

    def test_main_widget_opens(self):

        widget = MesmaWidget()
        widget.show()

        QTimer.singleShot(1000, app.closeAllWindows)
        app.exec_()

    def test_models_widget_opens(self):

        widget = MesmaModelsWidget(self.models_object)
        widget.show()

        QTimer.singleShot(1000, app.closeAllWindows)
        app.exec_()

    def test_levels_widget_opens(self):

        widget = LevelWidget(self.models_object, level=3)
        widget.show()

        QTimer.singleShot(1000, app.closeAllWindows)
        app.exec_()

    def test_multiple_images(self):

        widget = MesmaWidget()
        widget.browseLibrary.lineEdit().setText(self.roberts2017)
        widget.classDropDown.setCurrentText(self.r_class)
        widget.browseImage.lineEdit().setText('\"{}\" \"{}\"'.format(self.test_data1, self.test_data2))
        TestMesmaGUI.output = widget.browseOut.lineEdit().text()
        widget._run_mesma()
